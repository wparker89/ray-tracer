## README ##

### What is this repository for? ###

This is a ray tracing application written for my final project C++ course.  It utilises basic physics and vector maths to create images of simple shapes, by following the path a ray of light would take.

### How do I get set up? ###

* Clone the repository.
* Switch to the directory ray-tracer in terminal.
* Compile the program by running the "make" in the terminal.
* Run the program by executing the command "./ray-tracer".
* Basic shapes and images can be created by modifying the values in the various renderSceneMethods().

